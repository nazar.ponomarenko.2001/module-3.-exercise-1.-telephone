package ua.lv.ponomarenko.nazar.telephone;

public interface PhoneConnection {
    void call();
    void sendAMessage();
}
